package fr.fingarde.mineandglory.api.portal;

import fr.fingarde.mineandglory.api.selection.Selection;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

public abstract class Portal extends Selection
{
    static LinkedList<Portal> portals = new LinkedList<>();
    static HashMap<UUID, Integer> playerDelay = new HashMap<>();

    World world;


    /**
     * Constructs a new Portal with the given locations
     *
     * @param locations List of the corners ( must be an even number )
     * @see fr.fingarde.mineandglory.api.selection.Selection
     */
    public Portal(Location... locations)
    {
        super(locations);

        world = locations[0].getWorld();

        portals.add(this);
    }

    /**
     * An abstract method that must be redefined when instanced
     *
     * @param player The player who enters in the portal
     */
    public abstract void onEnter(Player player);

    /**
     * Setup the portal system ( Registers the event and set timeout for entering the portal)
     *
     * @param plugin An instance of the plugin
     */
    public static void setup(Plugin plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(new PortalListener(), plugin);

        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                for (Map.Entry<UUID, Integer> entry : playerDelay.entrySet())
                {
                    if (entry.getValue() == 0)
                    {
                        playerDelay.remove(entry.getKey());
                    }

                    entry.setValue(entry.getValue() - 1);
                }
            }
        }.runTaskTimerAsynchronously(plugin, 5, 5);
    }
}
