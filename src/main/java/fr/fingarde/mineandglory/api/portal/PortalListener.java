package fr.fingarde.mineandglory.api.portal;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.BoundingBox;

/**
 * @hidden
 */
public class PortalListener implements Listener
{
    @EventHandler
    public void onMove(PlayerMoveEvent event)
    {
        if (equals(event.getFrom(), event.getTo())) return;
        if (Portal.playerDelay.containsKey(event.getPlayer().getUniqueId())) return;

        Portal.portals.forEach(portal ->
        {
            if (!portal.world.equals(event.getTo().getWorld())) return;
            for (BoundingBox boundingBox : portal.boxes)
            {
                if (boundingBox.getCenter().toLocation(portal.world).distance(event.getTo()) > 100) continue;

                if (boundingBox.contains(event.getTo().toVector()))
                {
                    portal.onEnter(event.getPlayer());

                    Portal.playerDelay.put(event.getPlayer().getUniqueId(), 1);
                    break;
                }
            }
        });
    }

    private boolean equals(Location a, Location b)
    {
        if (a.getBlockX() != b.getBlockX() ||
                a.getBlockY() != b.getBlockY() ||
                a.getBlockZ() != b.getBlockZ()) return false;

        return true;
    }
}
