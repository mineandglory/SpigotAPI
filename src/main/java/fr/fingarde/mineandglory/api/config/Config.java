package fr.fingarde.mineandglory.api.config;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Config
{
    private static String pluginFolder = "plugins/MineAndGlory/";

    /**
     * Gets an YamlConfiguration of a desired file
     *
     * @param relativePath The relative path to the file
     * @return The config
     */
    public static YamlConfiguration getConfig(String relativePath)
    {
        try
        {
            String path = pluginFolder + relativePath;
            String folder = path.substring(0, path.lastIndexOf("/"));

            File folderFile = new File(folder);
            if (!folderFile.exists()) folderFile.mkdirs();

            YamlConfiguration config = new YamlConfiguration();

            File configFile = new File(folder, relativePath.replace(".yml", "") + ".yml");
            if (!configFile.exists()) return null;

            config.load(configFile);

            return config;
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (InvalidConfigurationException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Saves a config to a given file name
     *
     * @param config       The config to save
     * @param relativePath The relative path to the file
     */
    public static void saveConfig(YamlConfiguration config, String relativePath)
    {
        try
        {
            String path = pluginFolder + relativePath;
            String folder = path.substring(0, path.lastIndexOf("/"));

            File folderFile = new File(folder);
            if (!folderFile.exists()) folderFile.mkdirs();

            File configFile = new File(folder, relativePath.replace(".yml", "") + ".yml");
            if (!configFile.exists()) configFile.createNewFile();

            config.save(configFile);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static boolean fileExists(String relativePath)
    {
        String path = pluginFolder + relativePath.replace(".yml", "") + ".yml";

        return new File(path).exists();
    }
}
