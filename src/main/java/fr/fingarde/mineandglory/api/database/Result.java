package fr.fingarde.mineandglory.api.database;

import java.util.*;
import java.util.function.Consumer;

public class Result
{
    HashMap<String, Object> columns = new HashMap<>();

    /**
     * @hidden
     */
    Result()
    {
    }

    /**
     * Gets the names of the columns
     *
     * @return
     */
    public List<String> getColumns()
    {
        return new ArrayList<>(columns.keySet());
    }

    /**
     * Gets the value of a column as a String
     *
     * @param column The name of the concerned column
     * @return The value as a String
     */
    public String getAsString(String column)
    {
        return String.valueOf(columns.get(column));
    }

    /**
     * Gets the value of a column as an int
     *
     * @param column The name of the concerned column
     * @return The value as an int
     */
    public int getAsInt(String column)
    {
        return (int) columns.get(column);
    }

    /**
     * For each column
     *
     * @param action The action to do with the given column
     */
    public void forEach(Consumer<Object> action)
    {
        Objects.requireNonNull(action);

        columns.forEach((key, value) ->
        {
            action.accept(value);
        });
    }

    @Override
    public String toString()
    {
        return columns.toString();
    }
}
