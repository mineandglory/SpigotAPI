package fr.fingarde.mineandglory.api.database;

import com.zaxxer.hikari.HikariDataSource;
import fr.fingarde.mineandglory.api.config.Config;
import org.bukkit.configuration.file.YamlConfiguration;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class Database
{
    private static HikariDataSource source;

    Database()
    {
    }

    /**
     * Connects to the database using config values
     */
    public static void connectDatabase()
    {

        YamlConfiguration config = Config.getConfig("database");

        source = new HikariDataSource();
        source.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");

        source.addDataSourceProperty("serverName", config.getString("host"));
        source.addDataSourceProperty("port", config.getInt("port"));
        source.addDataSourceProperty("databaseName", config.getString("database"));
        source.addDataSourceProperty("user", config.getString("user"));
        source.addDataSourceProperty("password", config.getString("password"));

        source.addDataSourceProperty("allowPublicKeyRetrieval", true);
        source.addDataSourceProperty("verifyServerCertificate", false);
        source.addDataSourceProperty("useSSL", false);

        source.addDataSourceProperty("tcpKeepAlive", true);
        source.addDataSourceProperty("autoReconnect", true);
        source.addDataSourceProperty("connectTimeout", 300);

        source.addDataSourceProperty("characterEncoding", "utf8");
        source.addDataSourceProperty("useUnicode", "true");
    }

    /**
     * Execute a query to the database
     *
     * @param query  The query ( use %s for variable )
     * @param values The values used to replace the %s in the pattern
     * @return The list of results of the query
     * @see fr.fingarde.mineandglory.api.database.Result
     */
    public static List<Result> executeQuery(String query, Object... values)
    {
        for (Object obj : values)
        {
            query = query.replaceFirst("%s", obj.toString());
        }

        final String finalQuery = query;
        AtomicReference<List> list = new AtomicReference<>();
        new Thread(() ->
        {
            try (
                    Connection connection = source.getConnection();
                    PreparedStatement statement = connection.prepareStatement(finalQuery);
                    ResultSet queryResult = statement.executeQuery())
            {
                if (!queryResult.next())
                {
                    list.set(null);
                    return;
                }

                List<Result> results = new LinkedList<>();

                do
                {
                    Result row = new Result();

                    ResultSetMetaData metaData = queryResult.getMetaData();
                    for (int i = 1; i <= metaData.getColumnCount(); i++)
                    {
                        String name = metaData.getColumnName(i);

                        row.columns.put(name, queryResult.getObject(name));
                    }

                    results.add(row);
                } while (queryResult.next());

                list.set(results);

            } catch (SQLException throwables)
            {
                throwables.printStackTrace();
            }
        }).run();

        return list.get();
    }

    /**
     * Execute an update to the database
     *
     * @param query  The query ( use %s for variable )
     * @param values The values used to replace the %s in the pattern
     */
    public static void executeUpdate(String query, Object... values)
    {
        for (Object obj : values)
        {
            query = query.replaceFirst("%s", obj.toString());
        }

        final String finalQuery = query;
        new Thread(() ->
        {
            try (
                    Connection connection = source.getConnection();
                    PreparedStatement statement = connection.prepareStatement(finalQuery)
            )
            {
                statement.executeUpdate();
            } catch (SQLException throwables)
            {
                throwables.printStackTrace();
            }
        }).run();
    }

    public static void disconnectDatabase()
    {
        source.close();
    }
}
