package fr.fingarde.mineandglory.api.user;


import fr.fingarde.mineandglory.Main;
import fr.fingarde.mineandglory.api.database.Database;
import fr.fingarde.mineandglory.api.database.Result;
import fr.fingarde.mineandglory.api.group.Group;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class User
{
    public static ArrayList<User> users = new ArrayList<>();

    private UUID uuid;

    private Group group;

    private String nickname;
    private String prefix;
    private String suffix;

    private OfflinePlayer player;

    public User(UUID uuid)
    {
        this.uuid = uuid;
        List<Result> results = Database.executeQuery("SELECT * FROM tb_player WHERE pl_uuid = '%s'", uuid);

        if (results == null) return;

        Result result = results.get(0);
        group = Group.getByName(result.getAsString("pl_group"));

        prefix = result.getAsString("pl_prefix");
        suffix = result.getAsString("pl_suffix");
        nickname = result.getAsString("pl_nick");

        player = Bukkit.getOfflinePlayer(uuid);
    }

    public static void createNewUser(UUID uuid)
    {
        Database.executeUpdate("INSERT INTO tb_player(pl_uuid, pl_group, pl_first_join) values('%s', '%s', '%s')", uuid, Group.getDefaultGroup().getName(), new Date().getTime() / 1000);
    }


    // Getters and setters

    public static User getByUUID(UUID uuid)
    {
        for (User user : users)
        {
            if (user.uuid == uuid) return user;
        }

        return null;
    }

    public Group getGroup()
    {
        return group;
    }

    public String getNickname()
    {
        return nickname;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public String getSuffix()
    {
        return suffix;
    }

    public OfflinePlayer getPlayer()
    {
        return player;
    }

    // Functions

    public UUID getUUID()
    {
        return uuid;
    }

    public void loadName()
    {
        String nameNickname = player.getName();
        String namePrefix = "", nameSuffix = "";

        if (group.getPrefix() != null) namePrefix = group.getPrefix();
        if (group.getSuffix() != null) nameSuffix = group.getSuffix();

        if (prefix != "null") namePrefix = prefix;
        if (suffix != "null") nameSuffix = suffix;

        if (nickname != "null") nameNickname = nickname;

        if (namePrefix != "") namePrefix = namePrefix + " ";
        if (nameSuffix != "") nameSuffix = " " + nameSuffix;

        ((Player) player).setDisplayName(namePrefix + nameNickname + nameSuffix);
        ((Player) player).setPlayerListName(((Player) player).getDisplayName());

        Team team = Bukkit.getScoreboardManager().getMainScoreboard().getTeam(group.getTeamName());
        team.addEntry(player.getName());
    }

    public void loadPermissions()
    {
        if (group.getPermissions() != null)
        {
            PermissionAttachment attachment = ((Player) player).addAttachment(Main.getPlugin());

            for (String permission : group.getPermissions())
            {
                if (permission.startsWith("-"))
                {
                    permission = permission.substring(1);
                    attachment.setPermission(permission, false);
                } else
                {
                    attachment.setPermission(permission, true);
                }
            }
        }
    }
}
