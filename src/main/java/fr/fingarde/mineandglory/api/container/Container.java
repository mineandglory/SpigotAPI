package fr.fingarde.mineandglory.api.container;

import fr.fingarde.mineandglory.api.group.Group;
import fr.fingarde.mineandglory.api.utils.ColorUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.libs.it.unimi.dsi.fastutil.Hash;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;

import java.util.Date;
import java.util.LinkedList;
import java.util.Objects;

/**
 * Represents an inventory
 * Each time a player click or close the inventory, an event is called
 */
public abstract class Container
{
    static LinkedList<Container> containers = new LinkedList<>();

    int size;
    String pattern;
    long sessionId;

    /**
     * Constructs a new Container with the given size and pattern
     *
     * @param size    The size of the container
     * @param pattern The pattern for the name ( use %s for variable )
     */
    public Container(int size, String pattern)
    {
        this.size = size;
        this.pattern = pattern;
        this.sessionId = new Date().getTime();

        containers.add(this);
    }

    /**
     * Setup the container system ( Registers the event )
     *
     * @param plugin An instance of the plugin
     */
    public static void setup(Plugin plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(new ContainerListener(), plugin);
    }

    /**
     * Get a new inventory ( Creates a new inventory each time ) replaces the %s with de given values
     *
     * @param values The values used to replace the %s in the pattern
     * @return The inventory with the correct name
     */
    public Inventory getInventory(Object... values)
    {
        String name = pattern;
        for (Object obj : values)
        {
            name = name.replaceFirst("%s", obj.toString());
        }

        return Bukkit.createInventory(null, size, name + ColorUtil.hideChars("ctnr_id:" + sessionId));
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(pattern, size);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Container container = (Container) obj;

        return container.pattern.equals(this.pattern) && container.size == this.size;
    }

    /**
     * An abstract method that must be redefined when instanced
     *
     * @param clickEvent The event called when a player click in the inventory
     */
    public abstract void onClick(InventoryClickEvent clickEvent);

    /**
     * An abstract method that must be redefined when instanced
     *
     * @param closeEvent The event called when a player close the inventory
     */
    public abstract void onClose(InventoryCloseEvent closeEvent);
}
