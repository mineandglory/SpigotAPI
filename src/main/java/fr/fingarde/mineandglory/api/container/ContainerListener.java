package fr.fingarde.mineandglory.api.container;

import fr.fingarde.mineandglory.api.utils.ColorUtil;
import fr.fingarde.mineandglory.api.utils.RegexUtil;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

/**
 * @hidden
 */
public class ContainerListener implements Listener
{
    @EventHandler
    public void onClick(InventoryClickEvent event)
    {
        String title = ColorUtil.unhideChars(event.getView().getTitle());
        if(!title.contains("ctnr_id:")) return;

        long sessionId = Long.parseLong(RegexUtil.getMatches("ctnr_id:(\\d*)$", title).get(1));
        for (Container container : Container.containers)
        {
            if(container.sessionId == sessionId) {
                container.onClick(event);

                return;
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event)
    {
        String title = ColorUtil.unhideChars(event.getView().getTitle());
        if(!title.contains("ctnr_id:")) return;

        long sessionId = Long.parseLong(RegexUtil.getMatches("ctnr_id:(\\d*)$", title).get(1));
        for (Container container : Container.containers)
        {
            if(container.sessionId == sessionId) {
                container.onClose(event);
                Container.containers.remove(container);

                return;
            }
        }
    }
}
