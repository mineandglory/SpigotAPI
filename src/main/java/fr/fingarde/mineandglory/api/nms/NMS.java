package fr.fingarde.mineandglory.api.nms;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class NMS
{
    public static void sendPacketPlayOutBlockBreakAnimation(Player player, Block block, int value)
    {
        try
        {
            Class classPacketPlayOutBlockBreakAnimation = Class.forName("net.minecraft.server.v1_16_R1.PacketPlayOutBlockBreakAnimation");
            Class classBlockPosition = Class.forName("net.minecraft.server.v1_16_R1.BlockPosition");
            Class classCraftPlayer = Class.forName("org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer");
            Class classEntityPlayer = Class.forName("net.minecraft.server.v1_16_R1.EntityPlayer");
            Class classPlayerConnection = Class.forName("net.minecraft.server.v1_16_R1.PlayerConnection");
            Class classPacket = Class.forName("net.minecraft.server.v1_16_R1.Packet");

            Constructor constructorPacketPlayOutBlockBreakAnimation = classPacketPlayOutBlockBreakAnimation.getConstructor(int.class, classBlockPosition, int.class);
            Constructor constructorBlockPosition = classBlockPosition.getConstructor(int.class, int.class, int.class);

            Method methodGetHandle = classCraftPlayer.getDeclaredMethod("getHandle");
            Method methodSendPacket = classPlayerConnection.getDeclaredMethod("sendPacket", classPacket);
            Field fieldPlayerConnection = classEntityPlayer.getDeclaredField("playerConnection");

            Object craftPlayer = classCraftPlayer.cast(player);

            Object handle = methodGetHandle.invoke(craftPlayer);
            Object playerConnection = fieldPlayerConnection.get(handle);

            Object blockPosition = constructorBlockPosition.newInstance(block.getX(), block.getY(), block.getZ());
            Object packet = constructorPacketPlayOutBlockBreakAnimation.newInstance(0, blockPosition, value);

            methodSendPacket.invoke(playerConnection, packet);
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchFieldException classNotFoundException)
        {
            classNotFoundException.printStackTrace();
        }
    }

    public static void breakBlock(Player player, Block block)
    {
        try
        {
            Class classBlockPosition = Class.forName("net.minecraft.server.v1_16_R1.BlockPosition");
            Class classCraftPlayer = Class.forName("org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer");
            Class classEntityPlayer = Class.forName("net.minecraft.server.v1_16_R1.EntityPlayer");
            Class classPlayerInteractManager = Class.forName("net.minecraft.server.v1_16_R1.PlayerInteractManager");

            Constructor constructorBlockPosition = classBlockPosition.getConstructor(int.class, int.class, int.class);

            Method methodGetHandle = classCraftPlayer.getDeclaredMethod("getHandle");
            Field fieldPlayerInteractManager = classEntityPlayer.getDeclaredField("playerInteractManager");
            Method methodBreakBlock = classPlayerInteractManager.getDeclaredMethod("breakBlock", classBlockPosition);

            Object craftPlayer = classCraftPlayer.cast(player);
            Object handle = methodGetHandle.invoke(craftPlayer);

            Object playerInteractManager = fieldPlayerInteractManager.get(handle);

            Object blockPosition = constructorBlockPosition.newInstance(block.getX(), block.getY(), block.getZ());

            methodBreakBlock.invoke(playerInteractManager, blockPosition);
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchFieldException classNotFoundException)
        {
            classNotFoundException.printStackTrace();
        }
    }

    public static void playChestCloseAnimation(Block chest)
    {
        try
        {
            Class classBlockPosition = Class.forName("net.minecraft.server.v1_16_R1.BlockPosition");
            Class classCraftBlock = Class.forName("org.bukkit.craftbukkit.v1_16_R1.block.CraftBlock");
            Class classCraftWorld = Class.forName("org.bukkit.craftbukkit.v1_16_R1.CraftWorld");
            Class classWorldServer = Class.forName("net.minecraft.server.v1_16_R1.WorldServer");
            Class classBlock = Class.forName("net.minecraft.server.v1_16_R1.Block");

            Object craftWorld = classCraftWorld.cast(chest.getLocation().getWorld());
            Object craftBlock = classCraftBlock.cast(chest);

            Constructor constructorBlockPosition = classBlockPosition.getConstructor(int.class, int.class, int.class);

            Method methodGetHandleBlock = classCraftBlock.getDeclaredMethod("getNMSBlock");
            Method methodGetHandleWorld = classCraftWorld.getDeclaredMethod("getHandle");
            Method methodPlayBlockAction = classWorldServer.getDeclaredMethod("playBlockAction", classBlockPosition, classBlock, int.class, int.class);


            Object nmsBlock = methodGetHandleBlock.invoke(craftBlock);
            Object handleWorld = methodGetHandleWorld.invoke(craftWorld);
            Object blockPosition = constructorBlockPosition.newInstance(chest.getX(), chest.getY(), chest.getZ());


            methodPlayBlockAction.invoke(handleWorld, blockPosition, nmsBlock, 1, 0);

        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e)
        {
            e.printStackTrace();
        }
    }
}
