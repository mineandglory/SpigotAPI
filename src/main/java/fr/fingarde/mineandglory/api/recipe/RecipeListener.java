package fr.fingarde.mineandglory.api.recipe;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.FurnaceSmeltEvent;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @hidden
 */
public class RecipeListener implements Listener
{
    @EventHandler
    public void onFurnace(FurnaceSmeltEvent event)
    {
        for (Recipe recipe : Recipe.recipes.parallelStream().filter(recipe -> recipe instanceof FurnaceRecipe).collect(Collectors.toList()))
        {
            FurnaceRecipe furnaceRecipe = (FurnaceRecipe) recipe;

            event.getSource().getType();
            furnaceRecipe.ingredient.getType();
            if (furnaceRecipe.ingredient.getType() != event.getSource().getType()) continue;
            if (furnaceRecipe.ingredient.getItemMeta().getLocalizedName().equals(event.getSource().getItemMeta().getLocalizedName()))
            {
                event.setResult(recipe.result);
            }
        }
    }
}
