package fr.fingarde.mineandglory.api.recipe;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;

public class FurnaceRecipe extends Recipe
{

    ItemStack ingredient;

    public FurnaceRecipe(ItemStack result, ItemStack ingredient)
    {
        super(result);
        this.ingredient = ingredient;

        Bukkit.addRecipe(new org.bukkit.inventory.FurnaceRecipe(NamespacedKey.minecraft(ingredient.getType().toString().toLowerCase()), new ItemStack(Material.STONE), ingredient.getType(), 0, 128));
    }

    public ItemStack getIngredient()
    {
        return ingredient;
    }
}
