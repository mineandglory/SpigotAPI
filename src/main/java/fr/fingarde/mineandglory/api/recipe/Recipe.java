package fr.fingarde.mineandglory.api.recipe;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.LinkedList;

public abstract class Recipe
{
    static LinkedList<Recipe> recipes = new LinkedList<>();

    ItemStack result;

    Recipe(ItemStack result)
    {
        this.result = result;
        recipes.add(this);
    }

    public ItemStack getResult()
    {
        return result;
    }

    public static void setup(Plugin plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(new RecipeListener(), plugin);
    }
}
