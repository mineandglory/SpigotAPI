package fr.fingarde.mineandglory.api.group;


import fr.fingarde.mineandglory.Main;
import fr.fingarde.mineandglory.api.config.Config;
import org.bukkit.Bukkit;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.ArrayList;

public class Group
{
    private static ArrayList<Group> groups = new ArrayList<>();

    private String name;
    private String prefix;
    private String suffix;

    private String teamName;

    private ArrayList<String> inherits;
    private ArrayList<String> permissions = new ArrayList<>();

    private String chatColor;

    private boolean isDefault = false;

    private Group(String group, YamlConfiguration config)
    {
        this.name = group;
        if (groups.contains(this)) return;

        MemorySection data = (MemorySection) config.getConfigurationSection(group);

        prefix = data.getString("prefix");
        suffix = data.getString("suffix");

        teamName = "99_" + group + "_mag";
        if (data.contains("place_in_tab")) teamName = data.getString("place_in_tab") + "_" + group + "_mag";
        Bukkit.getScoreboardManager().getMainScoreboard().registerNewTeam(teamName);

        if (data.contains("permissions")) permissions = (ArrayList<String>) data.get("permissions");
        if (data.contains("inherit"))
        {
            inherits = (ArrayList<String>) data.get("inherit");

            for (String inherit : inherits)
            {
                Group inheritRank = getByName(inherit);
                if (inheritRank == null) inheritRank = new Group(inherit, config);

                permissions.addAll(inheritRank.getPermissions());
            }
        }

        if (data.contains("chat_color")) chatColor = data.getString("chat_color");
        if (data.contains("default")) isDefault = data.getBoolean("default");

        groups.add(this);
    }

    // Getters And Setters

    public String getName()
    {
        return name;
    }

    public ArrayList<String> getPermissions()
    {
        return permissions;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public String getSuffix()
    {
        return suffix;
    }

    public String getTeamName()
    {
        return teamName;
    }

    public String getChatColor()
    {
        return chatColor;
    }

    // Functions

    public static void loadGroups()
    {
        YamlConfiguration config = Config.getConfig("groups");

        for (String key : config.getKeys(false))
        {
            new Group(key, config);
        }

    }

    public static Group getByName(String name)
    {
        for (Group group : groups)
        {
            if (group.name.equalsIgnoreCase(name))
            {
                return group;
            }
        }

        return null;
    }

    public static Group getDefaultGroup()
    {
        for (Group group : groups)
        {
            if (group.isDefault)
            {
                return group;
            }
        }

        return null;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return name.equals(group.name);
    }

    @Override
    public int hashCode()
    {
        return name.hashCode();
    }
}