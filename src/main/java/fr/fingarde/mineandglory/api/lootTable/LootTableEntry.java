package fr.fingarde.mineandglory.api.lootTable;

import fr.fingarde.mineandglory.api.conditions.Condition;
import org.bukkit.inventory.ItemStack;

import java.util.LinkedList;
import java.util.List;

public class LootTableEntry
{
    private ItemStack item;
    private List<Condition> conditions;

    private int min;
    private int max;
    private float chance;

    /**
     * Constructs a new LootTableEntry with the given itemStack
     *
     * @param item The item stack ( amount doesn't matters )
     */
    public LootTableEntry(ItemStack item)
    {
        this.item = item;
        this.conditions = new LinkedList<>();
    }

    /**
     * Gets the item
     *
     * @return The item
     */
    public ItemStack getItem()
    {
        return item;
    }

    /**
     * Gets the list of conditions for obtaining the item
     *
     * @return The list of conditions
     */
    public List<Condition> getConditions()
    {
        return conditions;
    }

    /**
     * @return
     * @implNote Not implemented yet
     */
    public int getMin()
    {
        return min;
    }

    /**
     * @return
     * @implNote Not implemented yet
     */
    public int getMax()
    {
        return max;
    }

    /**
     * @return
     * @implNote Not implemented yet
     */
    public float getChance()
    {
        return chance;
    }

    /**
     * Add a condition to the list
     *
     * @param condition The condition to add
     * @return self
     */
    public LootTableEntry addCondition(Condition condition)
    {
        conditions.add(condition);
        return this;
    }

    /**
     * @return self
     * @implNote Not implemented yet
     */
    public LootTableEntry setMin(int min)
    {
        this.min = min;
        return this;
    }

    /**
     * @return self
     * @implNote Not implemented yet
     */
    public LootTableEntry setMax(int max)
    {
        this.max = max;
        return this;
    }

    /**
     * @return self
     * @implNote Not implemented yet
     */
    public LootTableEntry setChance(float chance)
    {
        this.chance = chance;
        return this;
    }
}

