package fr.fingarde.mineandglory.api.lootTable;

import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class LootTable
{
    private static HashMap<String, LootTable> lootTables = new HashMap<>();

    public LootTable(String name)
    {
        lootTables.put(name, this);
    }

    private List<LootTableEntry> entries = new LinkedList<>();

    /**
     * Adds an itemStack to the loot table
     *
     * @param entry An entry for a given itemStack
     * @return self
     * @see fr.fingarde.mineandglory.api.lootTable.LootTableEntry
     */
    public LootTable addEntry(LootTableEntry entry)
    {
        entries.add(entry);
        return this;
    }

    /**
     * Gets the list of items dropped by the loot tables with an itemStack
     *
     * @param itemStack
     * @return The list of items
     */
    public List<ItemStack> getLoot(ItemStack itemStack)
    {
        List<LootTableEntry> entries = new LinkedList<>();
        List<ItemStack> items = new LinkedList<>();

        this.entries.forEach(entry ->
        {
            boolean[] valid = {true};
            entry.getConditions().forEach(condition ->
            {
                if (!condition.isTrue(itemStack)) valid[0] = false;
            });

            if (valid[0])
            {
                entries.add(entry);
            }
        });

        entries.forEach(entry ->
                items.add(entry.getItem()));

        if (items.size() == 0) return null;
        return items;
    }

    public static LootTable getByName(String name)
    {
        if (!lootTables.containsKey(name)) return null;

        return lootTables.get(name);
    }

}
