package fr.fingarde.mineandglory.api.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtil
{
    public static float scaleDown(float value)
    {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(2, RoundingMode.DOWN);
        return bd.floatValue();
    }

    public static double scaleDown(double value)
    {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(2, RoundingMode.DOWN);
        return bd.doubleValue();
    }
}
