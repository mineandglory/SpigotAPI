package fr.fingarde.mineandglory.api.utils;

import net.md_5.bungee.api.ChatColor;

public class ColorUtil
{
    public static String removeColor(String str)
    {
        return str.replaceAll(ChatColor.COLOR_CHAR + ".", "");
    }

    public static String encodeAmperstamp(String str)
    {
        return str.replaceAll("&", String.valueOf(ChatColor.COLOR_CHAR));
    }

    public static String hideChars(String line)
    {
        StringBuilder builder = new StringBuilder();

        for(char c : line.toCharArray()){
            builder.append(ChatColor.COLOR_CHAR).append(c);
        }

        return builder.toString();
    }

    public static String unhideChars(String line)
    {
        return line.replaceAll(String.valueOf(ChatColor.COLOR_CHAR), "");
    }

}
