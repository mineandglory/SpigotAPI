package fr.fingarde.mineandglory.api.utils;

public class BooleanUtil
{
    public static boolean getOpposite(boolean value)
    {
        return (value ? false : true);
    }
}
