package fr.fingarde.mineandglory.api.utils;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil
{
    public static boolean matching(String regex, String str)
    {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        return matcher.find();
    }

    public static List<String> getMatches(String regex, String str)
    {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);

        List<String> result = new LinkedList<>();

        if (!matcher.find()) return null;
        for (int i = 0; i < matcher.groupCount() + 1; i++)
        {
            result.add(matcher.group(i));
        }

        return result;
    }

}
