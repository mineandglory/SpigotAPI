package fr.fingarde.mineandglory.api.utils;

import java.util.function.Consumer;

public class EnumUtil
{
    public static <E extends Enum<E>> void forEach(Class<E> enumClass, Consumer<E> action)
    {
        for (E e : enumClass.getEnumConstants())
            action.accept(e);
    }

    public static <E extends Enum<E>> E getValue(Class<E> enumClass, String value)
    {
        for (E c : enumClass.getEnumConstants())
        {
            if (c.name().equals(value))
            {
                return c;
            }
        }

        return null;
    }

    public static <E extends Enum<E>> E next(Class<E> enumClass, E value)
    {
        E[] values = enumClass.getEnumConstants();
        return values[(value.ordinal() + 1) % values.length];
    }

    public static <E extends Enum<E>> boolean contains(Class<E> enumClass, String value)
    {

        for (E c : enumClass.getEnumConstants())
        {
            if (c.name().equals(value))
            {
                return true;
            }
        }

        return false;
    }

}
