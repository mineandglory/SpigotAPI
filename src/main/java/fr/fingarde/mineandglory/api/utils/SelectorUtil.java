package fr.fingarde.mineandglory.api.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;

import java.util.*;
import java.util.stream.Collectors;

public class SelectorUtil
{
    public static List<Entity> parseSelector(String selector, Location location, String... extraOptions)
    {
        List<Entity> entities = new LinkedList<>();

        if (!selector.contains("@"))
        {
            if (RegexUtil.matching(".{8}-.{8}-.{12}-.{12}-.{12}", selector))
            {
                entities.add(Bukkit.getEntity(UUID.fromString(selector)));
            } else
            {
                entities.add(Bukkit.getPlayer(selector));
            }
        } else
        {
            List<String> options = new LinkedList<>();
            if (RegexUtil.matching("\\[.*\\]", selector))
            {
                options = new LinkedList<>(Arrays.asList(RegexUtil.getMatches("^.*\\[(.*)\\].*$", selector).get(1).split(",")));
            }

            options.addAll(Arrays.asList(extraOptions));

            switch (selector.substring(1, 2).toLowerCase())
            {
                case "e":
                    for (World world : Bukkit.getWorlds())
                    {
                        entities.addAll(world.getEntities());
                    }
                    break;
                case "a":
                    entities.addAll(Bukkit.getOnlinePlayers());
                    break;
                case "p":
                    entities.addAll(Bukkit.getOnlinePlayers());

                    options.add("sort=nearest");
                    options.add("limit=1");
                    break;
            }

            for (String option : options)
            {
                String optionType = option.split("=")[0];
                String value = option.split("=")[1];

                switch (optionType.toLowerCase())
                {
                    case "sort":
                        switch (value.toLowerCase())
                        {
                            case "nearest":
                            {
                                entities = entities.parallelStream().filter(entity -> entity.getWorld().equals(location.getWorld())).collect(Collectors.toList());
                                entities.sort((t, t1) -> (int) (t.getLocation().distanceSquared(location) - t1.getLocation().distanceSquared(location)));
                                break;
                            }
                        }
                }
            }

            for (Iterator<Entity> iterator = entities.iterator(); iterator.hasNext(); )
            {
                Entity entity = iterator.next();

                if (!checkOptions(entity, location, options))
                {
                    iterator.remove();
                }
            }

            for (String option : options)
            {
                String optionType = option.split("=")[0];
                String value = option.split("=")[1];

                switch (optionType.toLowerCase())
                {
                    case "limit":
                        entities.subList(Integer.valueOf(value), entities.size()).clear();
                        break;
                }
            }
        }

        return entities;
    }

    public static boolean checkOptions(Entity entity, Location location, List<String> options)
    {
        for (String option : options)
        {
            if (!checkOption(entity, location, option)) return false;
        }

        return true;
    }

    private static boolean checkOption(Entity entity, Location location, String option)
    {
        String optionType = option.split("=")[0];
        String value = option.split("=")[1];

        boolean reverseMode = false;
        if (value.startsWith("!"))
        {
            value = value.substring(1);
            reverseMode = true;
        }

        switch (optionType.toLowerCase())
        {
            case "type":
                if (!entity.getType().toString().equalsIgnoreCase(value)) return reverseMode;
                break;
            case "name":
                if (!entity.getName().equalsIgnoreCase(value)) return reverseMode;
                break;
            case "world":
                if (!entity.getWorld().equals(Bukkit.getWorld(value))) return reverseMode;
                break;
        }

        return BooleanUtil.getOpposite(reverseMode);
    }
}
