package fr.fingarde.mineandglory.api.selection;

import org.bukkit.Location;
import org.bukkit.util.BoundingBox;

import java.util.LinkedList;

public class Selection
{
    public LinkedList<BoundingBox> boxes;

    /**
     * Constructs a new Selection with the given locations
     *
     * @param locations List of the corners ( must be an even number )
     */
    public Selection(Location... locations)
    {
        if (locations.length % 2 == 1) throw new IllegalArgumentException("Locations must be even");
        boxes = new LinkedList<>();

        for (int i = 0; i < locations.length; i += 2)
        {
            Location a = locations[i];
            Location b = locations[i + 1];

            if (!a.getWorld().equals(b.getWorld()))
                throw new IllegalArgumentException("Location must be in the same world");

            if (a.getBlockX() == b.getBlockX())
            {
                if (a.getBlockZ() < b.getBlockZ())
                {
                    int temp = a.getBlockZ();
                    a.setZ(b.getBlockZ());
                    b.setZ(temp);
                }

                a.setZ(a.getBlockZ() + 1);
                b.setX(b.getBlockX() + 1);
            } else if (a.getBlockZ() == b.getBlockZ())
            {
                if (a.getBlockX() < b.getBlockX())
                {
                    int temp = a.getBlockX();
                    a.setX(b.getBlockX());
                    b.setX(temp);
                }

                a.setX(a.getBlockX() + 1);
                b.setZ(b.getBlockZ() + 1);
            } else
            {
                if (a.getBlockZ() > b.getBlockZ())
                {
                    int temp = a.getBlockZ();
                    a.setZ(b.getBlockZ());
                    b.setZ(temp);
                }

                if (a.getBlockX() > b.getBlockX())
                {
                    int temp = a.getBlockX();
                    a.setX(b.getBlockX());
                    b.setX(temp);
                }

                b.setX(b.getBlockX() + 1);
                b.setZ(b.getBlockZ() + 1);
            }

            if (a.getBlockY() > b.getBlockY())
            {
                int temp = a.getBlockY();
                a.setY(b.getBlockY());
                b.setY(temp);
            }

            b.setY(b.getBlockY() + 1);

            boxes.add(new BoundingBox(a.getBlockX(), a.getBlockY(), a.getBlockZ(), b.getBlockX(), b.getBlockY(), b.getBlockZ()));
        }
    }
}
