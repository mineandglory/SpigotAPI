package fr.fingarde.mineandglory.api.conditions;

import fr.fingarde.mineandglory.api.utils.RegexUtil;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class RegexConditionIgnoreCase implements Condition
{
    private String regex;

    public RegexConditionIgnoreCase(String regex)
    {
        this.regex = regex.toLowerCase();
    }

    @Override
    public boolean isTrue(ItemStack itemStack)
    {
        if (itemStack == null) return false;
        if (RegexUtil.matching(regex, itemStack.getType().name().toLowerCase())) return true;

        ItemMeta meta = itemStack.getItemMeta();
        return (RegexUtil.matching(regex, meta.getLocalizedName().toLowerCase()));
    }

}
