package fr.fingarde.mineandglory.api.conditions;

import org.bukkit.inventory.ItemStack;

public interface Condition
{
    boolean isTrue(ItemStack itemStack);
}