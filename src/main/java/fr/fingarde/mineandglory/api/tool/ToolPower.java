package fr.fingarde.mineandglory.api.tool;

import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

public enum ToolPower
{
    WOODEN,
    STONE,
    IRON,
    GOLD,
    DIAMOND,
    RUBY;

    public boolean isInferiorOrEqual(ItemStack item)
    {
        ToolPower itemToolPower = getToolPower(item);
        if (itemToolPower == null) return false;

        return itemToolPower.ordinal() >= this.ordinal();
    }

    public boolean isEquals(ItemStack item)
    {
        ToolPower itemToolPower = getToolPower(item);
        if (itemToolPower == null) return false;
        return this.equals(itemToolPower);
    }

    private ToolPower getToolPower(ItemStack item)
    {
        if (!item.getItemMeta().getLocalizedName().equals(""))
        {
            try
            {
                if (item.getItemMeta().getLocalizedName().contains("_"))
                {
                    return ToolPower.valueOf(item.getItemMeta().getLocalizedName().substring(0, item.getItemMeta().getLocalizedName().indexOf("_")));
                }
            } catch (IllegalArgumentException e)
            {
                return null;
            }

        } else
        {
            try
            {
                if (item.getType().name().contains("_"))
                {
                    return ToolPower.valueOf(item.getType().name().substring(0, item.getType().name().indexOf("_")));
                }
            } catch (IllegalArgumentException e)
            {
                return null;
            }
        }

        return null;
    }

}
