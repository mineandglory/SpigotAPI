package fr.fingarde.mineandglory.api.tool;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum ToolType
{
    AXE,
    SHOVEL,
    PICKAXE;

    public boolean isGoodToolType(ItemStack item)
    {
        if (item.getType() == Material.AIR) return false;
        if (item.getType().name().endsWith(this.name())) return true;
        if (item.getItemMeta().getLocalizedName().endsWith(this.name())) return true;

        return false;
    }
}
