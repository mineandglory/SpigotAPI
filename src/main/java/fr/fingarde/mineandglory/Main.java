package fr.fingarde.mineandglory;

import fr.fingarde.mineandglory.api.container.Container;
import fr.fingarde.mineandglory.api.database.Database;
import fr.fingarde.mineandglory.api.group.Group;
import fr.fingarde.mineandglory.api.portal.Portal;
import fr.fingarde.mineandglory.api.recipe.Recipe;
import fr.fingarde.mineandglory.api.user.User;
import fr.fingarde.mineandglory.defaults.DefaultConfig;
import fr.fingarde.mineandglory.defaults.DefaultDatabase;
import fr.fingarde.mineandglory.managers.CommandManager;
import fr.fingarde.mineandglory.managers.FurnaceRecipeManager;
import fr.fingarde.mineandglory.managers.ListenerManager;
import fr.fingarde.mineandglory.managers.LootTableManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Team;

import java.util.Set;
import java.util.logging.Logger;

public class Main extends JavaPlugin
{
    private static Main instance;
    private static Logger logger;

    public static Main getPlugin()
    {
        return instance;
    }

    public static Logger getConsole()
    {
        return logger;
    }

    private static void restoreUsers()
    {
        for (Player player : Bukkit.getOnlinePlayers())
        {
            User user = new User(player.getUniqueId());
            user.users.add(user);

            user.loadName();
            user.loadPermissions();
        }
    }

    private static void restoreTeams()
    {
        Set<Team> teams = Bukkit.getScoreboardManager().getMainScoreboard().getTeams();
        for (Team team : teams)
        {
            if (team.getName().endsWith("_mag"))
                team.unregister();
        }
    }

    @Override
    public void onEnable()
    {
        super.onEnable();

        instance = this;
        logger = getLogger();

        DefaultConfig.genConfig();
        Database.connectDatabase();
        DefaultDatabase.createTables();

        restoreTeams();
        Group.loadGroups();

        Portal.setup(this);
        Container.setup(this);
        Recipe.setup(this);

        CommandManager.registerCommands();
        ListenerManager.registerListeners();
        LootTableManager.registerLootTables();
        FurnaceRecipeManager.registerRecipes();

        new Portal(new Location(Bukkit.getWorld("world"), 1, 65, 18), new Location(Bukkit.getWorld("world"), -1, 67, 18))
        {
            @Override
            public void onEnter(Player player)
            {
                player.teleport(new Location(Bukkit.getWorld("world"), 0.5, 65, 64.5));
            }
        };

        restoreUsers();
    }

    @Override
    public void onDisable()
    {
        super.onDisable();

        Database.disconnectDatabase();
    }
}
