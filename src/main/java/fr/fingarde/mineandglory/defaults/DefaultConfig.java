package fr.fingarde.mineandglory.defaults;

import fr.fingarde.mineandglory.api.config.Config;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.ArrayList;

public class DefaultConfig
{
    public static void genConfig()
    {
        //          Database
        if (!Config.fileExists("database"))
        {
            YamlConfiguration database = new YamlConfiguration();

            database.set("host", "localhost");
            database.set("port", "port");
            database.set("database", "database");
            database.set("user", "root");
            database.set("password", "root");

            Config.saveConfig(database, "database");
        }


        if (!Config.fileExists("groups"))
        {
            //          Group
            YamlConfiguration group = new YamlConfiguration();

            /// -------------
            MemorySection visitor = (MemorySection) group.createSection("visitor");

            visitor.set("default", true);

            ArrayList<String> permisionsVisitor = new ArrayList<>();
            permisionsVisitor.add("chat.send");

            visitor.set("permissions", permisionsVisitor);

            /// -------------
            MemorySection member = (MemorySection) group.createSection("member");

            member.set("prefix", "§7Member");

            ArrayList<String> inherit = new ArrayList<>();
            inherit.add("visitor");

            member.set("inherit", inherit);


            ArrayList<String> permisionsMember = new ArrayList<>();
            permisionsMember.add("chat.mention.user");
            permisionsMember.add("world.interact");

            member.set("permissions", permisionsMember);

            /// -------------
            MemorySection owner = (MemorySection) group.createSection("owner");

            owner.set("prefix", "§c§lOwner");
            owner.set("chat_color", "§c");
            owner.set("place_in_tab", "1");

            Config.saveConfig(group, "groups");
        }
    }
}
