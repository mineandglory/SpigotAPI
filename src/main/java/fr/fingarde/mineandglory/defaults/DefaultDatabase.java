package fr.fingarde.mineandglory.defaults;

import fr.fingarde.mineandglory.api.database.Database;

public class DefaultDatabase
{
    public static void createTables()
    {
        Database.executeUpdate("CREATE TABLE IF NOT EXISTS tb_player(" +
                "pl_uuid VARCHAR(36) PRIMARY KEY," +
                "pl_group VARCHAR(36)," +
                "pl_prefix VARCHAR(64)," +
                "pl_suffix VARCHAR(64)," +
                "pl_nick VARCHAR(64)," +
                "pl_first_join NUMERIC(16)" +
                ");");

        Database.executeUpdate("CREATE TABLE IF NOT EXISTS tb_backpack(" +
                "bp_id VARCHAR(36) PRIMARY KEY," +
                "bp_content TEXT" +
                ");");
    }
}
