package fr.fingarde.mineandglory.commands;

import fr.fingarde.mineandglory.api.utils.EnumUtil;
import fr.fingarde.mineandglory.api.utils.SelectorUtil;
import fr.fingarde.mineandglory.custom.Items;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

public class GiveCommand implements CommandExecutor
{
    private String permission = "item.give";


    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args)
    {
        if (args.length == 1 && !(sender instanceof LivingEntity))
        {
            sender.sendMessage("Command can't be run by a non living entity with only 1 argument");
            return false;
        }

        Location location = new Location(Bukkit.getWorlds().get(0), 0, 0, 0);

        if (sender instanceof BlockCommandSender)
        {
            location = ((BlockCommandSender) sender).getBlock().getLocation();
        }
        if (sender instanceof LivingEntity)
        {
            location = ((LivingEntity) sender).getLocation();
        }
        if (sender instanceof Player)
        {
            if (!sender.hasPermission(permission))
            {
                sender.sendMessage("Your lacking permission " + permission);
            }
        }

        List<Entity> targets = (args.length > 1 ? SelectorUtil.parseSelector(args[0], location, "type=player") : Arrays.asList(new Entity[]{(Entity) sender}));

        String itemType = args[(args.length > 1 ? 1 : 0)].toUpperCase();

        ItemStack item;
        if (EnumUtil.contains(Items.class, itemType)) item = Items.getItem(Items.valueOf(itemType));
        else if (EnumUtil.contains(Material.class, itemType)) item = new ItemStack(Material.valueOf(itemType));
        else
        {
            sender.sendMessage("No item for entry " + itemType);
            return false;
        }

        for (Entity target : targets)
        {
            ((Player) target).getInventory().addItem(item);
        }

        return true;
    }
}
