package fr.fingarde.mineandglory.commands;

import fr.fingarde.mineandglory.api.container.Container;
import fr.fingarde.mineandglory.custom.Blocks;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @hidden
 */
public class TestCommand implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings)
    {
      /*  if(commandSender instanceof Entity) {
            Bukkit.broadcastMessage("Result :");
            SelectorUtil.parseSelector(strings[0], ((Entity) commandSender).getLocation()).forEach(entity -> Bukkit.broadcastMessage(entity.getName()));
        }*/

        Location loc = ((Player) commandSender).getLocation().clone();
        loc.setY(loc.getBlockY());
        Blocks.setBlock(loc, Blocks.BLUE_PORTAL_NS);

        /*Container c = new Container(54, "Shop page %s") {
            @Override
            public void onClick(InventoryClickEvent event) {
                String title = event.getView().getTitle();
                loadContainer((Player) event.getWhoClicked(), this, getInt(title) + 1);
            }

            @Override
            public void onClose(InventoryCloseEvent event) {
            }
        };

        loadContainer((Player) commandSender, c, 1);
*/
        return false;
    }

    public void loadContainer(Player p, Container c, int page)
    {
        Inventory inv = c.getInventory(page);

        inv.addItem(new ItemStack(Material.STONE));

        p.openInventory(inv);
    }

    public int getInt(String str)
    {
        Pattern p = Pattern.compile("^\\D*(\\d*).*$");
        Matcher m = p.matcher(str);

        if (!m.find()) return 0;

        return Integer.parseInt(m.group(1));
    }
}
