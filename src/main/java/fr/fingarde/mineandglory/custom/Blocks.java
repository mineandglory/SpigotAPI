package fr.fingarde.mineandglory.custom;

import fr.fingarde.mineandglory.api.tool.ToolPower;
import fr.fingarde.mineandglory.api.tool.ToolType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.MultipleFacing;
import org.bukkit.block.data.type.Tripwire;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum Blocks
{
    // PORTALS
    BLACK_PORTAL_EW(Material.TRIPWIRE, "disarmed"),
    BLACK_PORTAL_NS(Material.TRIPWIRE, "disarmed", "west"),

    BLUE_PORTAL_EW(Material.TRIPWIRE, "disarmed", "south"),
    BLUE_PORTAL_NS(Material.TRIPWIRE, "disarmed", "west", "south"),

    GREEN_PORTAL_EW(Material.TRIPWIRE, "disarmed", "north"),
    GREEN_PORTAL_NS(Material.TRIPWIRE, "disarmed", "west", "north"),

    RED_PORTAL_EW(Material.TRIPWIRE, "disarmed", "south", "north"),
    RED_PORTAL_NS(Material.TRIPWIRE, "disarmed", "west", "south", "north"),

    YELLOW_PORTAL_EW(Material.TRIPWIRE, "disarmed", "east"),
    YELLOW_PORTAL_NS(Material.TRIPWIRE, "disarmed", "west", "east"),

    WHITE_PORTAL_EW(Material.TRIPWIRE, "disarmed", "south", "east"),
    WHITE_PORTAL_NS(Material.TRIPWIRE, "disarmed", "west", "south", "east"),


    // SEEDS
    TOMATO_SEEDS(Material.CARROTS, "age=0"),
    LETTUCE_SEEDS(Material.POTATOES, "age=0"),
    RICE_SEEDS(Material.WHEAT, "age=0"),

    STRAWBERRY_SEEDS(Material.BEETROOTS, "age=0"),
    RASPBERRY_SEEDS(Material.BEETROOTS, "age=1"),

    CARROT_SEEDS(Material.CARROTS, "age=1"),
    POTATO_SEEDS(Material.POTATOES, "age=1"),
    WHEAT_SEEDS(Material.WHEAT, "age=1"),

    // ORE
    RUBY_ORE(Material.BROWN_MUSHROOM_BLOCK, ToolType.PICKAXE, ToolPower.DIAMOND, 3, "west"),
    RUBY_BLOCK(Material.BROWN_MUSHROOM_BLOCK, "south");

    private Material material;
    private List<String> data;
    private Integer resistance;
    private ToolType toolType;
    private ToolPower toolPower;

    Blocks(Material material, String... args)
    {
        this.material = material;
        this.data = Arrays.asList(args);
    }

    Blocks(Material material, ToolType toolType, ToolPower toolPower, Integer resistance, String... args)
    {
        this(material, args);
        this.toolType = toolType;
        this.toolPower = toolPower;
        this.resistance = resistance;
    }

    public static void setBlock(Location location, Blocks type)
    {
        Block block = location.getBlock();
        block.setType(type.material, false);

        switch (type.material)
        {
            case TRIPWIRE:
                Tripwire tripwire = (Tripwire) block.getBlockData();

                tripwire.setDisarmed(type.data.contains("attached") ? true : false);
                tripwire.setDisarmed(type.data.contains("disarmed") ? true : false);

                tripwire.setFace(BlockFace.EAST, type.data.contains("east") ? true : false);
                tripwire.setFace(BlockFace.NORTH, type.data.contains("north") ? true : false);
                tripwire.setFace(BlockFace.SOUTH, type.data.contains("south") ? true : false);
                tripwire.setFace(BlockFace.WEST, type.data.contains("west") ? true : false);

                block.setBlockData(tripwire, false);
                block.getState().update(false, false);
                break;
            case BROWN_MUSHROOM_BLOCK:
                MultipleFacing multipleFacing = (MultipleFacing) block.getBlockData();

                multipleFacing.setFace(BlockFace.EAST, type.data.contains("east") ? true : false);
                multipleFacing.setFace(BlockFace.NORTH, type.data.contains("north") ? true : false);
                multipleFacing.setFace(BlockFace.SOUTH, type.data.contains("south") ? true : false);
                multipleFacing.setFace(BlockFace.WEST, type.data.contains("west") ? true : false);
                multipleFacing.setFace(BlockFace.UP, type.data.contains("up") ? true : false);
                multipleFacing.setFace(BlockFace.DOWN, type.data.contains("down") ? true : false);

                block.setBlockData(multipleFacing, false);
                block.getState().update(false, false);
                break;
            case WHEAT:
            case CARROTS:
            case POTATOES:
            case BEETROOTS:
                Ageable ageable = (Ageable) block.getBlockData();
                ageable.setAge(Integer.valueOf(valueOrDefault(type.data, "age", "0")));

                block.setBlockData(ageable);
                block.getState().update();
                break;
        }
    }

    public static Blocks getFromBlock(Block block)
    {
        Optional<Blocks> blocks = Arrays.asList(Blocks.values()).stream()
                .filter(b -> b.material == block.getType())
                .filter(b ->
                {
                    switch (b.material)
                    {
                        case TRIPWIRE:
                            Tripwire tripwire = (Tripwire) block.getBlockData();

                            if (tripwire.isAttached() != b.data.contains("attached") ? true : false) break;
                            if (tripwire.isDisarmed() != b.data.contains("disarmed") ? true : false) break;

                            if (tripwire.hasFace(BlockFace.EAST) != b.data.contains("east") ? true : false) break;
                            if (tripwire.hasFace(BlockFace.NORTH) != b.data.contains("north") ? true : false) break;
                            if (tripwire.hasFace(BlockFace.SOUTH) != b.data.contains("south") ? true : false) break;
                            if (tripwire.hasFace(BlockFace.WEST) != b.data.contains("west") ? true : false) break;

                            return true;
                        case BROWN_MUSHROOM_BLOCK:
                            MultipleFacing multipleFacing = (MultipleFacing) block.getBlockData();

                            if (multipleFacing.hasFace(BlockFace.EAST) != b.data.contains("east") ? true : false) break;
                            if (multipleFacing.hasFace(BlockFace.NORTH) != b.data.contains("north") ? true : false)
                                break;
                            if (multipleFacing.hasFace(BlockFace.SOUTH) != b.data.contains("south") ? true : false)
                                break;
                            if (multipleFacing.hasFace(BlockFace.WEST) != b.data.contains("west") ? true : false) break;
                            if (multipleFacing.hasFace(BlockFace.UP) != b.data.contains("up") ? true : false) break;
                            if (multipleFacing.hasFace(BlockFace.DOWN) != b.data.contains("down") ? true : false) break;

                            return true;
                        case WHEAT:
                        case CARROTS:
                        case POTATOES:
                        case BEETROOTS:
                            Ageable ageable = (Ageable) block.getBlockData();
                            if (ageable.getAge() != Integer.valueOf(valueOrDefault(b.data, "age", "0"))) break;

                            return true;
                    }

                    return false;
                })
                .findFirst();

        return blocks.orElse(null);

    }

    private static String valueOrDefault(List<String> data, String key, String defaultValue)
    {
        for (String keys : data)
        {
            if (keys.startsWith(key + "="))
            {
                return keys.split("=")[1];
            }
        }

        return defaultValue;
    }

    private static String valueOrNull(List<String> data, String key)
    {
        return valueOrDefault(data, key, null);
    }

    public List<String> getData()
    {
        return data;
    }

    public Integer getResistance()
    {
        return resistance;
    }

    public ToolType getToolType()
    {
        return toolType;
    }

    public ToolPower getToolPower()
    {
        return toolPower;
    }

    public Material getMaterial()
    {
        return material;
    }



    public static class BreakState
    {
        public int neededDamage;
        public int damage;
        public Block block;

        public BreakState(int neededDamage, Block block)
        {
            this.neededDamage = neededDamage;
            this.damage = 0;
            this.block = block;
        }
    }

}
