package fr.fingarde.mineandglory.custom;

import fr.fingarde.mineandglory.api.utils.EnumUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public enum Items
{
    // PORTALS
    BLACK_PORTAL_NS(Material.EMERALD_ORE, 9001, "Black portal NS"),
    BLACK_PORTAL_EW(Material.EMERALD_ORE, 9002, "Black portal EW"),
    BLUE_PORTAL_NS(Material.EMERALD_ORE, 9003, "Blue portal NS"),
    BLUE_PORTAL_EW(Material.EMERALD_ORE, 9004, "Blue portal EW"),
    GREEN_PORTAL_NS(Material.EMERALD_ORE, 9005, "Green portal NS"),
    GREEN_PORTAL_EW(Material.EMERALD_ORE, 9006, "Green portal EW"),
    RED_PORTAL_NS(Material.EMERALD_ORE, 9007, "Red portal NS"),
    RED_PORTAL_EW(Material.EMERALD_ORE, 9008, "Red portal EW"),
    YELLOW_PORTAL_NS(Material.EMERALD_ORE, 9009, "Yellow portal NS"),
    YELLOW_PORTAL_EW(Material.EMERALD_ORE, 9010, "Yellow portal EW"),
    WHITE_PORTAL_NS(Material.EMERALD_ORE, 9011, "White portal NS"),
    WHITE_PORTAL_EW(Material.EMERALD_ORE, 9012, "White portal EW"),

    // SEEDS
    TOMATO_SEEDS(Material.WHEAT_SEEDS, 9001, "Tomato seeds"),
    LETTUCE_SEEDS(Material.WHEAT_SEEDS, 9002, "Lettuce seeds"),
    RICE_SEEDS(Material.WHEAT_SEEDS, 9003, "Rice seeds"),

    STRAWBERRY_SEEDS(Material.WHEAT_SEEDS, 9004, "Strawberry seeds"),
    RASPBERRY_SEEDS(Material.WHEAT_SEEDS, 9005, "Raspberry seeds"),

    WHEAT_SEEDS(Material.WHEAT_SEEDS, 9006, "Wheat seeds"),
    POTATO_SEEDS(Material.WHEAT_SEEDS, 9007, "Potato seeds"),
    CARROT_SEEDS(Material.WHEAT_SEEDS, 9008, "Carrot seeds"),

    // FOOD
    TOMATO(Material.APPLE, 9001, "Tomato"),
    LETTUCE(Material.APPLE, 9002, "Lettuce"),
    STRAWBERRY(Material.APPLE, 9003, "Strawberry"),
    RASPBERRY(Material.APPLE, 9004, "Raspberry"),
    RICE(Material.APPLE, 9005, "Rice"),

    // ITEMS
    BACKPACK(Material.CARROT_ON_A_STICK, 9001, "Backpack"),
    BIG_BACKPACK(Material.CARROT_ON_A_STICK, 9002, "Big Backpack"),
    ENDER_BACKPACK(Material.CARROT_ON_A_STICK, 9003, "Ender Backpack"),


    // ORES
    RUBY(Material.IRON_NUGGET, 16001, "Ruby"),
    RUBY_PICKAXE(Material.DIAMOND_PICKAXE, 16001, "Ruby Pickaxe"),

    RUBY_ORE(Material.EMERALD_ORE, 16001, "Ruby Ore"),
    RUBY_BLOCK(Material.EMERALD_ORE, 16002, "Ruby Block"),


    NULL(null, 0, null);

    private Material material;
    private int customModelData;
    private String title;
    private String[] data;

    Items(Material material, int customModelData, String title, String... data)
    {
        this.material = material;
        this.customModelData = customModelData;
        this.title = title;
        this.data = data;
    }

    public static ItemStack getItem(Items customItems)
    {
        ItemStack item = new ItemStack(customItems.material);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName("§r" + customItems.title);
        meta.setCustomModelData(customItems.customModelData);

        meta.setLocalizedName(customItems.name());
        item.setItemMeta(meta);
        return item;
    }

    public static Items getFromItem(ItemStack item)
    {
        if (item == null || item.getItemMeta() == null) return null;
        if (item.getItemMeta().getLocalizedName().equals("")) return null;

        return EnumUtil.getValue(Items.class, item.getItemMeta().getLocalizedName());
    }

    public String[] getData()
    {
        return data;
    }
}