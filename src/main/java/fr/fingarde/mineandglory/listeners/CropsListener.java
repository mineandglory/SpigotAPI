package fr.fingarde.mineandglory.listeners;

import fr.fingarde.mineandglory.api.lootTable.LootTable;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class CropsListener implements Listener
{
    @EventHandler
    public void onJump(PlayerInteractEvent event)
    {
        if (event.getAction() != Action.PHYSICAL) return;
        if (event.getClickedBlock().getType() != Material.FARMLAND) return;

        event.setCancelled(true);
    }

    @EventHandler
    public void onEntityJump(EntityInteractEvent event)
    {
        if (event.getEntity() instanceof Player) return;
        if (event.getBlock().getType() != Material.FARMLAND) return;

        event.setCancelled(true);
    }

    @EventHandler
    public void onGrow(BlockGrowEvent event)
    {
        Block block = event.getBlock();
        Ageable ageable = (Ageable) block.getBlockData();

        if (!isCrop(block.getType())) return;

        event.setCancelled(true);
        if (ageable.getAge() + 1 == ageable.getMaximumAge())
        {
            return;
        }

        ageable.setAge(ageable.getAge() + 2);

        block.setBlockData(ageable);
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event)
    {
        if (!isCrop(event.getBlock().getType())) return;

        String e = getType(event.getBlock());
        if (e == null) return;

        LootTable.getByName(e).getLoot(event.getPlayer().getInventory().getItemInMainHand()).forEach(itemStack -> event.getBlock().getWorld().dropItem(event.getBlock().getLocation(), itemStack));
        event.setDropItems(false);
    }

    @EventHandler
    public void onFlood(BlockFromToEvent event)
    {
        if (!isCrop(event.getBlock().getType())) return;

        String e = getType(event.getBlock());
        if (e == null) return;

        LootTable.getByName(e).getLoot(null).forEach(itemStack -> event.getBlock().getWorld().dropItem(event.getBlock().getLocation(), itemStack));
    }

    @EventHandler
    public void onClick(PlayerInteractEvent event)
    {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if (event.getPlayer().isSneaking()) return;

        if (event.getClickedBlock() == null) return;
        if (!isCrop(event.getClickedBlock().getType())) return;

        Ageable ageable = (Ageable) event.getClickedBlock().getBlockData();

        if (!isCrop(event.getClickedBlock().getType())) return;

        String e = getType(event.getClickedBlock());
        if (e == null) return;

        LootTable.getByName(e).getLoot(event.getItem()).forEach(itemStack -> event.getClickedBlock().getWorld().dropItem(event.getClickedBlock().getLocation(), itemStack));

        if (isBush(event.getMaterial())) ageable.setAge(ageable.getAge() - 2);
        else ageable.setAge(ageable.getAge() - 6);

        event.getClickedBlock().setBlockData(ageable);
    }

    @EventHandler
    public void onPiston(BlockPistonExtendEvent event)
    {
        for (Block block : event.getBlocks())
        {
            if (!isCrop(block.getType())) continue;

            String e = getType(event.getBlock());
            if (e == null) continue;

            LootTable.getByName(e).getLoot(null).forEach(itemStack -> event.getBlock().getWorld().dropItem(event.getBlock().getLocation(), itemStack));
        }
    }

    @EventHandler
    public void onVillager(EntityInteractEvent event)
    {
        if (!(event.getEntity() instanceof Villager)) return;
        if (!isCrop(event.getBlock().getType())) return;

        String e = getType(event.getBlock());
        if (e == null) return;

        event.setCancelled(true);
        LootTable.getByName(e).getLoot(null).forEach(itemStack -> event.getBlock().getWorld().dropItem(event.getBlock().getLocation(), itemStack));
    }


    private boolean isBush(Material material)
    {
        return material == Material.BEETROOTS;
    }

    private boolean isCrop(Material material)
    {
        switch (material)
        {
            case WHEAT:
            case POTATOES:
            case CARROTS:
            case BEETROOTS:
                return true;
        }

        return false;
    }

    private String getType(Block block)
    {
        Ageable ageable = (Ageable) block.getBlockData();
        Material type = block.getType();

        String e = null;

        switch (type)
        {
            case CARROTS:
                if (ageable.getAge() == 7)
                    return "carrot";
                else if (ageable.getAge() == 6)
                    return "tomato";
                break;
            case WHEAT:
                if (ageable.getAge() == 7)
                    return "wheat";
                else if (ageable.getAge() == 6)
                    return "rice";
                break;
            case POTATOES:
                if (ageable.getAge() == 7)
                    return "potato";
                else if (ageable.getAge() == 6)
                    return "lettuce";
                break;
            case BEETROOTS:
                if (ageable.getAge() == 3)
                    return "raspberry";
                else if (ageable.getAge() == 2)
                    return "strawberry";
                break;
        }

        return null;
    }
}
