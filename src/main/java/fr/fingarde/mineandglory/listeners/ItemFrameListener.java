package fr.fingarde.mineandglory.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

/**
 * @hidden
 */
public class ItemFrameListener implements Listener
{
    @EventHandler(priority = EventPriority.LOWEST)
    public void onClick(PlayerInteractAtEntityEvent event)
    {
        if (event.isCancelled()) return;

        if (event.getHand() != EquipmentSlot.HAND) return;
        if (!(event.getRightClicked() instanceof ItemFrame)) return;

        ItemFrame entity = (ItemFrame) event.getRightClicked();


        if (event.getPlayer().isSneaking() && entity.getItem().getType() != Material.AIR)
        {
            entity.setRotation(entity.getRotation().rotateCounterClockwise());

            entity.setVisible(!entity.isVisible());
            return;
        }

        if (!entity.isVisible())
        {
            entity.setRotation(entity.getRotation().rotateCounterClockwise());

            Block facing = entity.getLocation().getBlock().getRelative(entity.getAttachedFace());
            if (facing.getState() instanceof InventoryHolder)
            {
                Inventory inv = ((InventoryHolder) facing.getState()).getInventory();
                event.getPlayer().openInventory(inv);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onDamage(EntityDamageByEntityEvent event)
    {
        if (event.isCancelled()) return;

        if (!(event.getEntityType() == EntityType.ITEM_FRAME)) return;

        ItemFrame entity = (ItemFrame) event.getEntity();

        if (!entity.isVisible())
        {
            event.setCancelled(true);
        }
    }

    /* DoubleChest never close ( Spigot bug )
    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        event.getInventory().getViewers().clear();
        DoubleChest b = (DoubleChest) event.getInventory().getHolder();
    }
    */
}
