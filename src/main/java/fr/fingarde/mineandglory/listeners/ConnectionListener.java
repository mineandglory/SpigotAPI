package fr.fingarde.mineandglory.listeners;

import fr.fingarde.mineandglory.Main;
import fr.fingarde.mineandglory.api.database.Database;
import fr.fingarde.mineandglory.api.database.Result;
import fr.fingarde.mineandglory.api.user.User;
import fr.fingarde.mineandglory.api.utils.TitleUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.UUID;

public class ConnectionListener implements Listener
{
    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        event.setJoinMessage(null);


        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();

        List<Result> results = Database.executeQuery("SELECT * FROM tb_player WHERE pl_uuid = '%s'", uuid);

        if (results == null)
        {
            User.createNewUser(uuid);
            Bukkit.broadcastMessage(player.getDisplayName() + " §evient de rejoindre le serveur pour la première fois!");
        }


        User user = new User(uuid);
        User.users.add(user);

        user.loadName();
        user.loadPermissions();

        Bukkit.broadcastMessage("§a§l+§r " + player.getDisplayName());


        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                Location loc = player.getLocation();
                loc.setY(loc.getY() + 1.8);

                player.spawnParticle(Particle.TOTEM, loc, 150, 0, 0, 0, 0.4);
                TitleUtil.sendTitle(player, "§cMine And Glory");
            }
        }.runTaskLater(Main.getPlugin(), 5);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event)
    {
        event.setQuitMessage(null);

        User user = User.getByUUID(event.getPlayer().getUniqueId());
        User.users.remove(user);

        Bukkit.broadcastMessage("§c§l-§r " + event.getPlayer().getDisplayName());
    }
}
