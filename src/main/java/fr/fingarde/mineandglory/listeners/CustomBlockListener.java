package fr.fingarde.mineandglory.listeners;

import fr.fingarde.mineandglory.Main;
import fr.fingarde.mineandglory.api.nms.NMS;
import fr.fingarde.mineandglory.custom.Blocks;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.data.type.Tripwire;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;

import static org.bukkit.Material.*;

public class CustomBlockListener implements Listener
{

    static HashMap<Player, Integer> timeMap = new HashMap<>();
    static HashMap<Player, Blocks.BreakState> breakMap = new HashMap<>();

    /*
    @EventHandler
    public void onClick(PlayerInteractEvent event) {
        if(event.getHand() != EquipmentSlot.HAND) return;
        if(event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if(event.getClickedBlock() == null) return;
        if(event.getItem() == null) return;
        if(event.getItem().getType() != Material.GOLD_NUGGET) return;
        if(event.getItem().getItemMeta().getLocalizedName().equals("")) return;

        if(! event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) event.getItem().setAmount(0);

        Block b = event.getClickedBlock().getRelative(event.getBlockFace());

        Blocks.setBlock(b.getLocation(), Blocks.valueOf(event.getItem().getItemMeta().getLocalizedName()));
    }*/

    public static void update()
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                for (Map.Entry<Player, Integer> entry : timeMap.entrySet())
                {
                    if (entry.getValue() <= 0)
                    {
                        NMS.sendPacketPlayOutBlockBreakAnimation(entry.getKey(), breakMap.get(entry.getKey()).block, 0);

                        timeMap.remove(entry.getKey());
                        breakMap.remove(entry.getKey());
                    } else entry.setValue(entry.getValue() - 1);
                }
            }
        }.runTaskTimerAsynchronously(Main.getPlugin(), 1, 1);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlace(BlockPlaceEvent event)
    {
        if (event.isCancelled()) return;

        if (event.getItemInHand().getType() == Material.EMERALD_ORE)
        {
            if (event.getItemInHand().getItemMeta().getLocalizedName().equals("")) return;

            Blocks.setBlock(event.getBlockPlaced().getLocation(), Blocks.valueOf(event.getItemInHand().getItemMeta().getLocalizedName()));
        }
        if (event.getItemInHand().getType() == Material.WHEAT_SEEDS)
        {
            if (event.getItemInHand().getItemMeta().getLocalizedName().equals("")) return;
            Blocks.setBlock(event.getBlockPlaced().getLocation(), Blocks.valueOf(event.getItemInHand().getItemMeta().getLocalizedName()));
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onBreak(BlockBreakEvent event)
    {
        if (event.isCancelled()) return;
        if (event.getBlock().getType() == Material.TRIPWIRE && ((Tripwire) event.getBlock().getBlockData()).isDisarmed())
        {
            event.setCancelled(true);
            event.getBlock().setType(AIR, false);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onInteract(PlayerInteractEvent event)
    {
        if (event.getHand() != EquipmentSlot.HAND) return;
        if (event.getAction() != Action.LEFT_CLICK_BLOCK) return;
        if (event.getClickedBlock().getType() != BROWN_MUSHROOM_BLOCK) return;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onAnime(PlayerAnimationEvent event)
    {
        if (!breakMap.containsKey(event.getPlayer())) return;

        event.getPlayer().removePotionEffect(PotionEffectType.SLOW_DIGGING);
        event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 2, 110, false, false, false));

        Blocks.BreakState breakState = breakMap.get(event.getPlayer());
        if (timeMap.containsKey(event.getPlayer()))
        {
            timeMap.put(event.getPlayer(), 2);

            int state = (breakState.damage * 9 / breakState.neededDamage);
            if (state == 10)
            {
                Event event1 = new BlockBreakEvent(breakState.block, event.getPlayer());
                Bukkit.getServer().getPluginManager().callEvent(event1);
                if (!((BlockBreakEvent) event1).isCancelled())
                    breakState.block.breakNaturally(event.getPlayer().getInventory().getItemInMainHand());
                return;
            }

            if (state != ((breakState.damage + 1) * 9 / breakState.neededDamage))
            {
                NMS.sendPacketPlayOutBlockBreakAnimation(event.getPlayer(), breakState.block, state);
            }

            breakState.damage++;

            // Not compatible with spigot 1.16.1
            /*
            try {
                Class classCraftPlayer = Class.forName("org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer");
                Class classEntityPlayer = Class.forName("net.minecraft.server.v1_16_R1.EntityPlayer");
                Class classPlayerConnection = Class.forName("net.minecraft.server.v1_16_R1.PlayerConnection");
                Class classPacketPlayOutAnimation = Class.forName("net.minecraft.server.v1_16_R1.PacketPlayInArmAnimation");

                Object handle = classCraftPlayer.getDeclaredMethod("getHandle").invoke(classCraftPlayer.cast(event.getPlayer()));
                Object playerConnection = classEntityPlayer.getDeclaredField("playerConnection").get(handle);
                Object hand = Class.forName("net.minecraft.server.v1_16_R1.EnumHand").getEnumConstants()[0];
                Object packet = classPacketPlayOutAnimation.getConstructor(Class.forName("net.minecraft.server.v1_16_R1.EnumHand")).newInstance(hand);

                Method methodSendPacket = classPlayerConnection.getDeclaredMethod("sendPacket", Class.forName("net.minecraft.server.v1_16_R1.Packet"));
                methodSendPacket.invoke(playerConnection, packet);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
            */
        }

    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onClick(BlockDamageEvent event)
    {
        if (breakMap.containsKey(event.getPlayer()))
        {
            breakMap.remove(event.getPlayer());
            timeMap.remove(event.getPlayer());
        }

        // https://minecraft.gamepedia.com/Breaking
        ItemStack itemInHand = event.getPlayer().getInventory().getItemInMainHand();
        Blocks block = Blocks.getFromBlock(event.getBlock());

        if (block == null) return;
        if (block.getResistance() == null) return;

        double speedMultiplier = 1;
        double seconds;

        if (block.getToolType().isGoodToolType(itemInHand) && block.getToolPower().isInferiorOrEqual(itemInHand))
            seconds = block.getResistance() * 1.5;
        else
            seconds = block.getResistance() * 5;

        if (itemInHand.getEnchantmentLevel(Enchantment.DIG_SPEED) != 0 && block.getToolType().isGoodToolType(itemInHand) && block.getToolPower().isInferiorOrEqual(itemInHand))
            speedMultiplier += (Math.pow(itemInHand.getEnchantmentLevel(Enchantment.DIG_SPEED), 2 - (block.getResistance() / 2)) + 1);

        if (event.getPlayer().hasPotionEffect(PotionEffectType.FAST_DIGGING))
            speedMultiplier *= 1 + (0.3 * event.getPlayer().getPotionEffect(PotionEffectType.FAST_DIGGING).getAmplifier());

        if (event.getPlayer().hasPotionEffect(PotionEffectType.SLOW_DIGGING) && event.getPlayer().getPotionEffect(PotionEffectType.SLOW_DIGGING).getAmplifier() != 110)
            speedMultiplier /= 3 ^ event.getPlayer().getPotionEffect(PotionEffectType.SLOW_DIGGING).getAmplifier();

        seconds /= speedMultiplier;
        seconds /= 3;

        if (event.getPlayer().getLocation().getBlock().getRelative(0, 1, 0).getType() == WATER)
            seconds *= 5;

        NMS.sendPacketPlayOutBlockBreakAnimation(event.getPlayer(), event.getBlock(), 0);

        timeMap.put(event.getPlayer(), 2);
        breakMap.put(event.getPlayer(), new Blocks.BreakState((int) Math.floor(seconds * 20), event.getBlock()));
    }

    @EventHandler
    public void onUpdate(BlockPhysicsEvent event)
    {
        if (event.getBlock().getType() == Material.TRIPWIRE)
        {
            if (!((Tripwire) event.getBlock().getBlockData()).isDisarmed()) return;

            event.setCancelled(true);

            new BukkitRunnable()
            {
                @Override
                public void run()
                {
                    for (int x = -1; x < 2; x++)
                        for (int y = -1; y < 2; y++)
                            for (int z = -1; z < 2; z++)
                                event.getBlock().getRelative(x, y, z).getState().update(false, false);
                }
            }.runTaskLater(Main.getPlugin(), 0);
        }
        else if (event.getBlock().getType() == Material.BROWN_MUSHROOM_BLOCK)
        {
            event.setCancelled(true);
        }

    }
}
