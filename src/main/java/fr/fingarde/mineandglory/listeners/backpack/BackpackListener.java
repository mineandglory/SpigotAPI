package fr.fingarde.mineandglory.listeners.backpack;

import fr.fingarde.mineandglory.api.container.Container;
import fr.fingarde.mineandglory.api.database.Database;
import fr.fingarde.mineandglory.api.serializer.ItemSerializer;
import fr.fingarde.mineandglory.api.utils.ItemUtil;
import fr.fingarde.mineandglory.custom.Items;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


public class BackpackListener implements Listener
{
    private static List<UUID> openedBackPack = new ArrayList<>();

    @EventHandler
    public void onInteract(PlayerInteractEvent event)
    {
        // Check if the action is right click
        if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && event.getClickedBlock().getType().isInteractable()) return;

        // Check if the item is a backpack
        if (event.getItem() == null) return;
        String localizedName = event.getItem().getItemMeta().getLocalizedName();
        if (!localizedName.endsWith("BACKPACK")) return;

        // If ender backpack simply open ender chest
        if (localizedName.equals(Items.ENDER_BACKPACK.name()))
        {
            event.getPlayer().openInventory(event.getPlayer().getEnderChest());
            return;
        }

        // Data relative
        long lastOpenedTimestamp = new Date().getTime();
        UUID uuid;
        ItemStack[] content;


        // If backpack was never opened
        if (!ItemUtil.hasTag(event.getItem(), "backpack_uuid"))
        {
            uuid = UUID.randomUUID();
            ItemUtil.setTag(event.getItem(), "backpack_uuid", uuid);

            content = new ItemStack[] {};
            Database.executeUpdate("INSERT INTO tb_backpack(bp_id) VALUES('%s')", uuid);
        } else
        {
            uuid = UUID.fromString(ItemUtil.getTag(event.getItem(), "backpack_uuid"));
            // Check if backpack already opened
            if (openedBackPack.contains(uuid)) return;

            // Load content
            String contentHash = Database.executeQuery("SELECT bp_content FROM tb_backpack WHERE bp_id = '%s'", uuid).get(0).getAsString("bp_content");
            content = ItemSerializer.deserializeArray(contentHash);
        }

        // Set last opened timestamp
        ItemUtil.setTag(event.getItem(), "backpack_lastOpened", lastOpenedTimestamp);

        // Set size and name of the backpack
        int size = (localizedName.equals(Items.BACKPACK.name()) ? 27 : 54);
        String name = (localizedName.equals(Items.BACKPACK.name()) ? "Backpack" : "Big Backpack");

        // Event relative
        UUID finalUuid = uuid;

        Container c = new Container(size, name)
        {
            @Override
            public void onClick(InventoryClickEvent clickEvent)
            {
                // If clicked item is a backpack
                boolean isBackpack = false;
                if (clickEvent.getClick() == ClickType.NUMBER_KEY)
                    isBackpack = true;
                else if (clickEvent.getCurrentItem() != null)
                {
                    String localizedNameCurrent = clickEvent.getCurrentItem().getItemMeta().getLocalizedName();
                    if (localizedNameCurrent.endsWith("BACKPACK"))
                    {
                        isBackpack = true;
                    }
                }
                if (clickEvent.getCursor() != null && clickEvent.getCursor().getItemMeta() != null)
                {
                    String localizedNameCursor = clickEvent.getCursor().getItemMeta().getLocalizedName();
                    if (localizedNameCursor.endsWith("BACKPACK"))
                    {
                        isBackpack = true;
                    }
                }

                // Cancel the event
                clickEvent.setCancelled(isBackpack);
            }

            @Override
            public void onClose(InventoryCloseEvent closeEvent)
            {

                if (!ItemUtil.hasTag(event.getItem(), "backpack_uuid")) return;
                if (!event.getItem().equals(closeEvent.getPlayer().getInventory().getItemInMainHand())) return;

                long itemInHandLastOpenedTimestamp = Long.parseLong(ItemUtil.getTag(event.getItem(), "backpack_lastOpened"));
                UUID itemInHandUuid = UUID.fromString(ItemUtil.getTag(event.getItem(), "backpack_uuid"));

                if(lastOpenedTimestamp != itemInHandLastOpenedTimestamp) return;
                if(!finalUuid.equals(itemInHandUuid)) return;

                Database.executeUpdate("UPDATE tb_backpack SET bp_content = '%s' WHERE bp_id = '%s'", ItemSerializer.serializeArray(closeEvent.getInventory().getContents()), finalUuid);
                openedBackPack.remove(finalUuid);

            }
        };

        // Add the items
        Inventory inv = c.getInventory();

        for (ItemStack itemStack : content)
        {
            if (itemStack != null)
                inv.addItem(itemStack);
        }


        // Open the inventory
        event.getPlayer().openInventory(inv);
        openedBackPack.add(uuid);
    }
}
