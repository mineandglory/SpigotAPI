package fr.fingarde.mineandglory.managers;

import fr.fingarde.mineandglory.Main;
import fr.fingarde.mineandglory.listeners.*;
import fr.fingarde.mineandglory.listeners.backpack.BackpackListener;
import org.bukkit.event.Listener;

public class ListenerManager
{
    public static void registerListeners()
    {
        register(new ConnectionListener());
        register(new ChatListener());
        register(new CustomBlockListener());
        register(new BlockPathListener());
        register(new CropsListener());
        register(new ItemFrameListener());
        register(new BackpackListener());
    }

    private static void register(Listener listener)
    {
        Main.getPlugin().getServer().getPluginManager().registerEvents(listener, Main.getPlugin());
    }
}
