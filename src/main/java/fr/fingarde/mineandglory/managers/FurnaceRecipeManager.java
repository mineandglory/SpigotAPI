package fr.fingarde.mineandglory.managers;

import fr.fingarde.mineandglory.api.recipe.FurnaceRecipe;
import fr.fingarde.mineandglory.custom.Items;

public class FurnaceRecipeManager
{
    public static void registerRecipes()
    {
        new FurnaceRecipe(Items.getItem(Items.BACKPACK), Items.getItem(Items.LETTUCE));
    }
}
