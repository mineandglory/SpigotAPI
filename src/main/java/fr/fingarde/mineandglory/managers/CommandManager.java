package fr.fingarde.mineandglory.managers;

import fr.fingarde.mineandglory.Main;
import fr.fingarde.mineandglory.commands.GiveCommand;
import fr.fingarde.mineandglory.commands.TestCommand;
import org.bukkit.command.CommandExecutor;

public class CommandManager
{
    public static void registerCommands()
    {
        registerCommand("test", new TestCommand());
        registerCommand("give", new GiveCommand());
    }

    public static void registerCommand(String command, CommandExecutor commandExecutor)
    {
        Main.getPlugin().getCommand(command).setExecutor(commandExecutor);
    }

}
