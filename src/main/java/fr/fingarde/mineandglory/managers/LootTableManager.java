package fr.fingarde.mineandglory.managers;

import fr.fingarde.mineandglory.Main;
import fr.fingarde.mineandglory.api.lootTable.LootTable;
import fr.fingarde.mineandglory.api.lootTable.LootTableEntry;
import fr.fingarde.mineandglory.custom.Items;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;

public class LootTableManager
{
    public static void registerLootTables()
    {
        new LootTable("tomato").addEntry(new LootTableEntry(Items.getItem(Items.TOMATO)));
        new LootTable("rice").addEntry(new LootTableEntry(Items.getItem(Items.RICE)));
        new LootTable("lettuce").addEntry(new LootTableEntry(Items.getItem(Items.LETTUCE)));
        new LootTable("raspberry").addEntry(new LootTableEntry(Items.getItem(Items.RASPBERRY)));
        new LootTable("strawberry").addEntry(new LootTableEntry(Items.getItem(Items.STRAWBERRY)));
        new LootTable("wheat").addEntry(new LootTableEntry(new ItemStack(Material.WHEAT)));
        new LootTable("carrot").addEntry(new LootTableEntry(new ItemStack(Material.CARROT)));
        new LootTable("potato").addEntry(new LootTableEntry(new ItemStack(Material.POTATO)));

        Main.getPlugin().getServer().addRecipe(new FurnaceRecipe(NamespacedKey.minecraft("oui"), new ItemStack(Material.COAL), Material.STONE, 1, 10));
    }
}
